﻿using UnityEngine;
using System.Collections;
using Soomla.Store;


public class WallBehaviour : MonoBehaviour {

	private Animator anim;

	// Use this for initialization
	void Start () {
		StoreEvents.OnItemPurchased += OnItemPurchased;
		anim = gameObject.GetComponent<Animator> ();
		anim.SetFloat ("wallType", DataLoader.fWallType);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnItemPurchased(PurchasableVirtualItem pvi)
	{
		
		switch (pvi.ItemId) 
		{
		case PurchasedAssets.WALL01_ITEM_ID:
		{
			DataLoader.fWallType = 1f;
			break;
		}
		case PurchasedAssets.WALL02_ITEM_ID:
		{
			DataLoader.fWallType = 2f;
			break;
		}
		case PurchasedAssets.WALL03_ITEM_ID:
		{
			DataLoader.fWallType = 3f;
			break;
		}
		case PurchasedAssets.WALL04_ITEM_ID:
		{
			DataLoader.fWallType = 4f;
			break;
		}
		}
		anim.SetFloat ("wallType", DataLoader.fWallType);
		
	}
}
