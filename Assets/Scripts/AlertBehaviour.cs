﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlertBehaviour : MonoBehaviour {

	public Transform alertOk;
	public Transform alertOkCancel;
	public Text oMessage;
	public Transform oWin;
	public static AlertBehaviour oAlert;


	// Use this for initialization
	void Start () {
		if (oAlert == null)
		{
			oAlert = this;
		}
	}

	// type of alert dialog 
	// 0 - dialog ok without result
	// 1- ok cancel dialog
	public void OpenAlert(int type, string message)
	{
		oWin.gameObject.SetActive(true);
		oMessage.text = message;
		int alertType = type;
		switch (alertType)
		{
			case 1:
			{
				alertOk.gameObject.SetActive(false);
				alertOkCancel.gameObject.SetActive(true);
				break;
			}
			default:
			{
				alertOk.gameObject.SetActive(true);
				alertOkCancel.gameObject.SetActive(false);
				break;
			}
		}
	}

	// 2 - ok result
	// 3 - cancel result
	public void OnResult(int result)
	{
		TimeEvents.OnDialogResult(result);
		Debug.Log("result: " +result);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
