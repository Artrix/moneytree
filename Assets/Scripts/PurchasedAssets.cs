using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class PurchasedAssets : IStoreAssets{
	
	/// <summary>
	/// see parent.
	/// </summary>
	public int GetVersion() {
		return 15;
	}
	
	/// <summary>
	/// see parent.
	/// </summary>
	public VirtualCurrency[] GetCurrencies() {
		return new VirtualCurrency[]{COINS_CURRENCY, RUBIES_CURRENCY};
	}
	//	
	//	/// <summary>
	//	/// see parent.
	//	/// </summary>
	public VirtualGood[] GetGoods() {
		return new VirtualGood[] { 	SHOVEL_GOOD, 
									FERTILIZER01_GOOD, FERTILIZER02_GOOD, FERTILIZER03_GOOD, 
									POT01_GOOD, POT02_GOOD,
									SPRAY01_GOOD, SPRAY02_GOOD, SPRAY03_GOOD,
									AUTOWATER01_GOOD, AUTOWATER02_GOOD, AUTOWATER03_GOOD,
									DECOR01_GOOD, DECOR02_GOOD,
									GIFT01_GOOD, GIFT02_GOOD,
									WALL01_GOOD, WALL02_GOOD, WALL03_GOOD, WALL04_GOOD,
									TEST_GOOD
									};
	}
	//	
	//	/// <summary>
	//	/// see parent.
	//	/// </summary>
	public VirtualCurrencyPack[] GetCurrencyPacks() {
		return new VirtualCurrencyPack[] {COINS01_PACK, COINS02_PACK, COINS03_PACK, COINS04_PACK, COINS05_PACK, COINS06_PACK};
	}
	//	
	//	/// <summary>
	//	/// see parent.
	//	/// </summary>
	public VirtualCategory[] GetCategories() {
		return new VirtualCategory[]{INVENTORY_CATEGORY};
	}
	//	
	//	/// <summary>
	//	/// see parent.
	//	/// </summary>
	public NonConsumableItem[] GetNonConsumableItems() {
		return new NonConsumableItem[]{};
	}


	//	
	//	/** Static Final Members **/
	//	
	//	
	public const string SHOVEL_ITEM_ID   = "garden_shovel";
	//	
	public const string FERTILIZER01_ITEM_ID   = "fertilizer_01";
	//	
	public const string FERTILIZER02_ITEM_ID   = "fertilizer_02";
	//	
	public const string FERTILIZER03_ITEM_ID   = "fertilizer_03";
	//	
	public const string POT01_ITEM_ID   = "pot_01";
	//	
	public const string POT02_ITEM_ID   = "pot_02";
	//	
	public const string SPRAY01_ITEM_ID   = "spray_01";
	//	
	public const string SPRAY02_ITEM_ID   = "spray_02";
	//	
	public const string SPRAY03_ITEM_ID   = "spray_03";
	//	
	public const string AUTOWATER01_ITEM_ID   = "autowater_01";
	//	
	public const string AUTOWATER02_ITEM_ID   = "autowater_02";
	//	
	public const string AUTOWATER03_ITEM_ID   = "autowater_03";
	//	
	public const string DECOR01_ITEM_ID   = "decor_01";
	//	
	public const string DECOR02_ITEM_ID   = "decor_02";
	//	
	public const string GIFT01_ITEM_ID   = "gift_01";
	//	
	public const string GIFT02_ITEM_ID   = "gift_02";

	public const string WALL01_ITEM_ID   = "wallpaper_01";
	//	
	public const string WALL02_ITEM_ID   = "wallpaper_02";
	//	
	public const string WALL03_ITEM_ID   = "wallpaper_03";
	//	
	public const string WALL04_ITEM_ID   = "wallpaper_04";
	//	

	public const string TEST_ITEM_ID = "test";

	public const string COINS01_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_01";

	public const string COINS02_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_02";
	public const string COINS03_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_03";
	public const string COINS04_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_04";
	public const string COINS05_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_05";

	public const string COINS06_ITEM_ID   = "com.home.moneytree.inapp.coins_offer_06";

	public const string COINS_PACK01_ITEM_ID   = "coins_offer_01";
	
	public const string COINS_PACK02_ITEM_ID   = "coins_offer_02";
	public const string COINS_PACK03_ITEM_ID   = "coins_offer_03";
	public const string COINS_PACK04_ITEM_ID   = "coins_offer_04";
	public const string COINS_PACK05_ITEM_ID   = "coins_offer_05";
	
	public const string COINS_PACK06_ITEM_ID   = "coins_offer_06";


	public const string COINS_ITEM_ID   = "local.coins";

	public const string RUBIES_ITEM_ID   = "local.rubies";
	

	public static string[] goodIds = {
		COINS_ITEM_ID,  		//0
		COINS_PACK01_ITEM_ID, 
		COINS_PACK02_ITEM_ID,
		COINS_PACK03_ITEM_ID,
		COINS_PACK04_ITEM_ID,
		COINS_PACK05_ITEM_ID,
		COINS_PACK06_ITEM_ID,
		"",
		"",
		"",				
		SHOVEL_ITEM_ID,			//10
		FERTILIZER01_ITEM_ID,
		FERTILIZER02_ITEM_ID,
		FERTILIZER03_ITEM_ID,
		POT01_ITEM_ID,			//14
		POT02_ITEM_ID,			//15
		SPRAY01_ITEM_ID,
		SPRAY02_ITEM_ID,
		SPRAY03_ITEM_ID,
		AUTOWATER01_ITEM_ID,
		AUTOWATER02_ITEM_ID,	//20
		AUTOWATER03_ITEM_ID,
		DECOR01_ITEM_ID,
		DECOR02_ITEM_ID,
		GIFT01_ITEM_ID,
		GIFT02_ITEM_ID,			//25
		WALL01_ITEM_ID,
		WALL02_ITEM_ID,
		WALL03_ITEM_ID,
		WALL04_ITEM_ID

	} ;


	/** Virtual Currencies **/
	
	public static VirtualCurrency COINS_CURRENCY = new VirtualCurrency(
		"Coins",										// name
		"",												// description
		COINS_ITEM_ID									// item id
		);

	public static VirtualCurrency RUBIES_CURRENCY = new VirtualCurrency(
		"Rubies",										// name
		"",												// description
		RUBIES_ITEM_ID									// item id
		);

	
	
	/** Virtual Currency Packs **/
	
	public static VirtualCurrencyPack COINS01_PACK = new VirtualCurrencyPack(
		"10000 Coins",                                   // name
		"Buy 10000 coins",                       // description
		COINS_PACK01_ITEM_ID,                                   // item id
		10000,												// number of currencies in the pack
		COINS_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS01_ITEM_ID, 0.99)
		);

	public static VirtualCurrencyPack COINS02_PACK = new VirtualCurrencyPack(
		"60000 Coins",                                   // name
		"Buy 65000 coins",                       // description
		COINS_PACK02_ITEM_ID,                                   // item id
		65000,												// number of currencies in the pack
		COINS_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS02_ITEM_ID, 4.99)
		);

	public static VirtualCurrencyPack COINS03_PACK = new VirtualCurrencyPack(
		"150000 Coins",                                   // name
		"Buy 150000 coins",                       // description
		COINS_PACK03_ITEM_ID,                                   // item id
		150000,												// number of currencies in the pack
		COINS_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS03_ITEM_ID, 9.99)
		);

	public static VirtualCurrencyPack COINS04_PACK = new VirtualCurrencyPack(
		"100 Rubies",                                   // name
		"Buy 100 rubies",                       // description
		COINS_PACK04_ITEM_ID,                                   // item id
		100,												// number of currencies in the pack
		RUBIES_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS04_ITEM_ID, 0.99)
		);

	public static VirtualCurrencyPack COINS05_PACK = new VirtualCurrencyPack(
		"600 Rubies",                                   // name
		"Buy 600 ubies",                       // description
		COINS_PACK05_ITEM_ID,                                   // item id
		600,												// number of currencies in the pack
		RUBIES_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS05_ITEM_ID, 4.99)
		);

	public static VirtualCurrencyPack COINS06_PACK = new VirtualCurrencyPack(
		"1250 Rubies",                                   // name
		"Buy 1250 ubies",                       // description
		COINS_PACK06_ITEM_ID,                                   // item id
		1250,												// number of currencies in the pack
		RUBIES_ITEM_ID,                        // the currency associated with this pack
		new PurchaseWithMarket(COINS06_ITEM_ID, 9.99)
		);

	//	
//	public static VirtualCurrencyPack FIFTYMUFF_PACK = new VirtualCurrencyPack(
//		"50 Muffins",                                   // name
//		"Test cancellation of an item",                 // description
//		"muffins_50",                                   // item id
//		50,                                             // number of currencies in the pack
//		MUFFIN_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//		new PurchaseWithMarket(FIFTYMUFF_PACK_PRODUCT_ID, 1.99)
//		);
//	
//	public static VirtualCurrencyPack FOURHUNDMUFF_PACK = new VirtualCurrencyPack(
//		"400 Muffins",                                  // name
//		"Test purchase of an item",                 	// description
//		"muffins_400",                                  // item id
//		400,                                            // number of currencies in the pack
//		MUFFIN_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//		new PurchaseWithMarket(FOURHUNDMUFF_PACK_PRODUCT_ID, 4.99)
//		);
//	
//	public static VirtualCurrencyPack THOUSANDMUFF_PACK = new VirtualCurrencyPack(
//		"1000 Muffins",                                 // name
//		"Test item unavailable",                 		// description
//		"muffins_1000",                                 // item id
//		1000,                                           // number of currencies in the pack
//		MUFFIN_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//		new PurchaseWithMarket(THOUSANDMUFF_PACK_PRODUCT_ID, 8.99)
//		);
	
	/** Virtual Goods **/
	
	public static VirtualGood SHOVEL_GOOD = new SingleUseVG(
		"shovel",                                       		// name
		"Buy garden shovel", // description
		SHOVEL_ITEM_ID,                                       		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 0)); // the way this virtual good is purchased

	
	public static VirtualGood FERTILIZER01_GOOD = new SingleUseVG(
		"fertilizer 01",                                         			// name
		"Buy fertilizer Econom", // description
		FERTILIZER01_ITEM_ID,                                          		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 3000)); // the way this virtual good is purchased

	public static VirtualGood FERTILIZER02_GOOD = new SingleUseVG(
		"fertilizer 02",                                   		// name
		"Buy fertilizer",	 		// description
		FERTILIZER02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(RUBIES_ITEM_ID, 45)); // the way this virtual good is purchased

	public static VirtualGood FERTILIZER03_GOOD = new SingleUseVG(
		"fertilizer 03",                                   		// name
		"Buy fertilizer",	 		// description
		FERTILIZER03_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(RUBIES_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood POT01_GOOD = new SingleUseVG(
		"pot 01",                                   		// name
		"01",	 		// description
		POT01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 4000)); // the way this virtual good is purchased

	public static VirtualGood POT02_GOOD = new SingleUseVG(
		"pot 02",                                   		// name
		"02",	 		// description
		POT02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 40000)); // the way this virtual good is purchased

	public static VirtualGood SPRAY01_GOOD = new SingleUseVG(
		"spray 01",                                   		// name
		"01",	 		// description
		SPRAY01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood SPRAY02_GOOD = new SingleUseVG(
		"spray 02",                                   		// name
		"02",	 		// description
		SPRAY02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased
	
	public static VirtualGood SPRAY03_GOOD = new SingleUseVG(
		"spray 03",                                   		// name
		"03",	 		// description
		SPRAY03_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood AUTOWATER01_GOOD = new SingleUseVG(
		"autowater 01",                                   		// name
		"7",	 		// description
		AUTOWATER01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood AUTOWATER02_GOOD = new SingleUseVG(
		"autowater 02",                                   		// name
		"15",	 		// description
		AUTOWATER02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood AUTOWATER03_GOOD = new SingleUseVG(
		"autowater 03",                                   		// name
		"30",	 		// description
		AUTOWATER03_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood DECOR01_GOOD = new SingleUseVG(
		"decor 01",                                   		// name
		"01",	 		// description
		DECOR01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood DECOR02_GOOD = new SingleUseVG(
		"decor 02",                                   		// name
		"02",	 		// description
		DECOR02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood GIFT01_GOOD = new SingleUseVG(
		"gift 01",                                   		// name
		"01",	 		// description
		GIFT01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood GIFT02_GOOD = new SingleUseVG(
		"gift 02",                                   		// name
		"02",	 		// description
		GIFT02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased


	public static VirtualGood TEST_GOOD = new SingleUseVG(
		"test",                                   		// name
		"test",	 		// description
		TEST_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 0)); // the way this virtual good is purchased

	public static VirtualGood WALL01_GOOD = new SingleUseVG(
		"wall 01",                                   		// name
		"Wallpaper",	 		// description
		WALL01_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood WALL02_GOOD = new SingleUseVG(
		"wall 01",                                   		// name
		"Wallpaper",	 		// description
		WALL02_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood WALL03_GOOD = new SingleUseVG(
		"wall 01",                                   		// name
		"Wallpaper",	 		// description
		WALL03_ITEM_ID,                                   		// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	public static VirtualGood WALL04_GOOD = new SingleUseVG(
		"wall 01",                                   		// name
		"Wallpaper",	 		// description
		WALL04_ITEM_ID,// item id
		new PurchaseWithVirtualItem(COINS_ITEM_ID, 100)); // the way this virtual good is purchased

	//	
//	public static VirtualGood CREAMCUP_GOOD = new SingleUseVG(
//		"Cream Cup",                                        		// name
//		"Increase bakery reputation with this original pastry",   	// description
//		"cream_cup",                                        		// item id
//		new PurchaseWithVirtualItem(MUFFIN_CURRENCY_ITEM_ID, 50));  // the way this virtual good is purchased
//	
//	
//	/** Virtual Categories **/
	// The muffin rush theme doesn't support categories, so we just put everything under a general category.
	public static VirtualCategory INVENTORY_CATEGORY = new VirtualCategory(
		"Inventory", new List<string>(new string[] { SHOVEL_ITEM_ID, FERTILIZER01_ITEM_ID, FERTILIZER02_ITEM_ID, FERTILIZER03_ITEM_ID, SPRAY01_ITEM_ID, SPRAY02_ITEM_ID, SPRAY03_ITEM_ID, 
														AUTOWATER01_ITEM_ID, AUTOWATER02_ITEM_ID, AUTOWATER03_ITEM_ID })
	);
//	


	
	/** Market MANAGED Items **/
	
	//	public static VirtualGood VIRTUAL_COINS  = new SingleUseVG(
	//		"coins",
	//		"Test purchase of MANAGED item.",
	//		"coins",
	//		new PurchaseWithMarket(HUNDRED_COINS_ITEM_ID, 0.99)
	//
	//		);

	
}