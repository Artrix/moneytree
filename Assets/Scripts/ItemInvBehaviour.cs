﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

public class ItemInvBehaviour : MonoBehaviour {

	public int good_Id;
	private string goodId;
	private SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
		goodId = PurchasedAssets.goodIds[good_Id];
		sprite = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		int value = -1;
		bool isValue = LocalStoreInfo.GoodsBalances.TryGetValue (goodId, out value);
		if (isValue && value > 0) {
			sprite.enabled = true;
		} else if (value == 0) {
			sprite.enabled = false;
		}
	}

}
