using UnityEngine;
using System.Collections;

public class LightBehaviour : MonoBehaviour {

	public Light lightElement; 
	public float nightIntensity;
	public float dayIntensity;
	private const int _NIGHT_END = 5;
	private const int _DAY_START = 9;
	private const int _DAY_END = 18;
	private const int _NIGHT_START = 23;



	// Use this for initialization
	void Start () {
		TimeEvents.OnHourChanged += OnHourChanged;
//		anim = gameObject.GetComponent<Animator> ();


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnHourChanged(int hour)
	{

//		anim.SetFloat ("hour",(float)hour);
		if (hour <= _DAY_END && hour >= _DAY_START) {
			lightElement.intensity = dayIntensity;
		} else if (hour <= _NIGHT_END || hour >= _NIGHT_START) {
			lightElement.intensity = nightIntensity;
		} else if (hour >= _DAY_END && hour <= _NIGHT_START) {
			lightElement.intensity = Mathf.Abs(dayIntensity - nightIntensity ) * (hour - _DAY_END) / (_NIGHT_START - _DAY_END) + nightIntensity;
		} else if (hour >= _NIGHT_END && hour <= _DAY_START) {
			lightElement.intensity = Mathf.Abs(dayIntensity - nightIntensity ) * (hour - _NIGHT_END) / (_DAY_START - _NIGHT_END) + nightIntensity;
		}

	}

}
