﻿using UnityEngine;
using System.Collections;
using Soomla.Store;

public class AutoWaterBehaviour : MonoBehaviour {

	public int days = 7;

	public SpriteRenderer spriteInPot;

	// Use this for initialization
	void Start () {
		int good_Id = gameObject.GetComponent<ItemInvBehaviour>().good_Id;
		TimeEvents.OnHourGo += OnHourGo;
		TimeEvents.OnDataLoaded += OnDataLoaded;
		if (DataLoader.iAutoWaterDays == days) {
			spriteInPot.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnHourGo()
	{

		if (DataLoader.iAutoWaterDays > 0 &&
		    (DataLoader.timeAutoWaterEnd - System.DateTime.UtcNow).TotalSeconds <= 0)
		{
			HideAutoWater();
		}

	}

	public void OnDataLoaded()
	{
		
		spriteInPot.enabled = (DataLoader.iAutoWaterDays == days);
		
	}

	public void AutoWater()
	{
		if (DataLoader.iAutoWaterDays > 0 && DataLoader.iAutoWaterDays != days) {
			GameObject[] autoWaters = GameObject.FindGameObjectsWithTag("autoWater");
			if (autoWaters != null && autoWaters.Length > 0)
			{
				for (int i = 0; i < autoWaters.Length; i++)
				{
					autoWaters[i].GetComponent<AutoWaterBehaviour>().HideAutoWater();
				}
			}
		}
		switch (DataLoader.iSpeed) {
		case 1:
			DataLoader.timeAutoWaterEnd = System.DateTime.UtcNow.AddDays (days);
			break;
		case 2:
			DataLoader.timeAutoWaterEnd = System.DateTime.UtcNow.AddMinutes(days * 24);
			break;
		case 3:
			DataLoader.timeAutoWaterEnd = System.DateTime.UtcNow.AddSeconds(days * 24);
			break;
		}
		//		DataLoader.timeAutoWaterEnd = System.DateTime.UtcNow.AddDays (days);
		//DataLoader.timeAutoWaterEnd = System.DateTime.UtcNow.AddSeconds(days * 24);
		DataLoader.iAutoWaterDays = days;
	}

	public void HideAutoWater()
	{
		if (DataLoader.iAutoWaterDays == days) {
			DataLoader.iAutoWaterDays = 0;
			spriteInPot.enabled = false;
		}
	}

}
