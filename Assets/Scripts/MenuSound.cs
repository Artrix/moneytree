using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuSound : MonoBehaviour {

	public AudioListener audioLst;

	public GameObject soundOff;

	private Image soundOn;
	// Use this for initialization

	void Awake ()
	{

		}

	void Start () {
		soundOn = gameObject.GetComponent<Image> ();
		if (DataLoader.iSoundOff == 1) {
			audioLst.enabled = false;

		}
		soundOn.enabled = audioLst.enabled;
		soundOff.SetActive (!audioLst.enabled);		

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnActivate()
	{
		audioLst.enabled = !audioLst.enabled;
		DataLoader.iSoundOff =  audioLst.enabled ? 0 : 1;
		soundOn.enabled = audioLst.enabled;
		soundOff.SetActive(!audioLst.enabled);		
	
	}
}
