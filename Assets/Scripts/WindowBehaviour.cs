using UnityEngine;
using System.Collections;

public class WindowBehaviour : MonoBehaviour {

	public Transform winClose;

	private SpriteRenderer winOpen;



	// Use this for initialization
	void Start () {

		winOpen = gameObject.GetComponent<SpriteRenderer> ();
		if (DataLoader.iWindowClosed == 1) {
						winClose.gameObject.SetActive (true);
						winOpen.enabled = false;
				}
		TimeEvents.OnHourGoEnd += OnHourGo;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnHourGo()
	{
		int winTemp = MenuThermometr._aiTempWin [System.DateTime.Now.Month - 1];
		if (DataLoader.iWindowClosed == 0 && DataLoader.fTemperature > winTemp) {
			DataLoader.fTemperature -= Random.Range(1,3);
			if (DataLoader.fTemperature < winTemp)
				DataLoader.fTemperature = winTemp;
		}
	}

	public void OnClick()
	{
		bool open = winOpen.enabled;
		DataLoader.iWindowClosed =  open ? 1 : 0;
		winClose.gameObject.SetActive (open);
		winOpen.enabled = !open;
	}
}
