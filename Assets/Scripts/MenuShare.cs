﻿using UnityEngine;
using System.Collections;
using Facebook.MiniJSON;
using System.Collections.Generic;

public class MenuShare : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick()
	{
		if (FB.IsLoggedIn)
		{
		List<object> FriendSelectorFilters = new List<object>();
		string filter = "app_non_users"; 
		string mes = "Take money from MoneyTree!"; 
		FriendSelectorFilters.Add(filter);
		FB.AppRequest(
			mes,
			null,
			FriendSelectorFilters,
			null,
			null,
			"{}",
			"Invite friends to play MoneyTree with me!",
			callback: appRequestCallback
			);

		}
	}

	private void appRequestCallback (FBResult result)
	{
		Util.Log("appRequestCallback");
		if (result != null)
		{
			var responseObject = Json.Deserialize(result.Text) as Dictionary<string, object>;
			object obj = 0;
			if (responseObject.TryGetValue ("cancelled", out obj))
			{
				Debug.Log("Request cancelled");
			}
			else if (responseObject.TryGetValue ("request", out obj))
			{

				Debug.Log("Request sent");
			}
		}
	}
}
