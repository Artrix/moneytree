using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MenuCam : MonoBehaviour {
	
	public Color color;
	private Color prevColor;
	private bool isWaiting;
	public Transform WinAlert;
	public Text alertText;
	public AlertBehaviour alert;
	public Animation animFlash;
	// Use this for initialization
	void Start () {
		color = DataLoader.colorText;
		prevColor = color;
		isWaiting = false;
		TimeEvents.OnDialogResult += OnDialogResult;

	}


	// Update is called once per frame
	void Update () {
			prevColor = color - prevColor;
			DataLoader.colorText += prevColor;
			prevColor = color;
	}


	public void MakeShot()
	{
		if (DataLoader.userViewId.CompareTo(FB.UserId) == 0)
		{
			if (animFlash != null) {
				animFlash.Play ();
			}
			StartCoroutine(DelayAnim());
			
		} else{
			AlertBehaviour.oAlert.OpenAlert( 0, "Other tree can not be photographed");
		}

	}

	public void OnDialogResult(int result)
	{

		if (isWaiting && result == 2)
		{
			isWaiting = false;
			StartCoroutine (TakeScreenshot());

		}
	}


	IEnumerator DelayAnim(){

		if (animFlash != null) {
			yield return new WaitForSeconds (animFlash.clip.length);
		}
		isWaiting = true;
		AlertBehaviour.oAlert.OpenAlert(1, "Post photo in FB album?");
	}



	private IEnumerator TakeScreenshot()
	{
		yield return new WaitForEndOfFrame();
		
		var width = (int)(Screen.height * 0.8f);
		var height = (int)(Screen.height * 0.6f);

		var tex = new Texture2D(width + 20, height + 26, TextureFormat.RGB24, false);
		var tex2 = Texture2D.whiteTexture;
		tex2.Resize (width + 20, height + 26);
		Color[] colors = tex2.GetPixels ();
		tex.SetPixels (colors);

		// Read screen contents into the texture
		tex.ReadPixels(new Rect((int)(Screen.width * 0.5f - Screen.height * 0.4f), (int)(Screen.height * 0.25f), width, height), 10, 13);
		tex.Apply();
		byte[] screenshot = tex.EncodeToPNG();
		
		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", screenshot, "MoneyTree"+(DataLoader.iGrowth+1)+".png");
		wwwForm.AddField("message", "Look! It is my MoneyTree about " + (DataLoader.iGrowth+1) + " cm!");
		
		FB.API("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
	}

	protected void Callback(FBResult result)
	{
		// Some platforms return the empty string instead of null.
		if (!String.IsNullOrEmpty (result.Error))
		{
			Debug.Log("Error Response:\n" + result.Error);
		}
		else if (!String.IsNullOrEmpty (result.Text))
		{
			Debug.Log("Success Response:\n" + result.Text);
		}
		else if (result.Texture != null)
		{
			Debug.Log("Success Response: texture\n");
		}
		else
		{
			Debug.Log("Empty Response\n");
		}
	}
}
