using UnityEngine;
using System.Collections;
using Soomla.Store;

public class PotBehaviour : MonoBehaviour {

	private Animator anim;



	// Use this for initialization
	void Start () {
		StoreEvents.OnItemPurchased += OnItemPurchased;
		anim = gameObject.GetComponent<Animator> ();
		anim.SetFloat ("potType", DataLoader.fPotType);
		TimeEvents.OnHourChanged += OnHourChanged;
		TimeEvents.OnHourGo += OnHourGo;
	}
	
	// Update is called once per frame
	void Update () {


	}

	public void OnItemPurchased(PurchasableVirtualItem pvi)
	{

		switch (pvi.ItemId) 
		{
			case PurchasedAssets.POT01_ITEM_ID:
			{
				DataLoader.fPotType = 1f;
				break;
			}
			case PurchasedAssets.POT02_ITEM_ID:
			{
				DataLoader.fPotType = 3f;
				break;
			}
		}
		anim.SetFloat ("potType", DataLoader.fPotType);

	}

	public void OnHourChanged(int hour)
	{
//		DataLoader.fHumidity -= (int)Random.Range (2, 3);
//		if (DataLoader.isAutoWater) 
//		{
//			if (DataLoader.fHumidity < DataLoader._OPTIM_HUMIDITY){
//					DataLoader.fHumidity += 2;
//				}
//
//		} 
	}

	public void OnHourGo()
	{
		// humidity changes
		if (DataLoader.iAutoWaterDays == 0)
				DataLoader.fHumidity -= 2.5f;

		if (DataLoader.iAutoWaterDays > 0 && DataLoader.fHumidity < DataLoader._OPTIM_HUMIDITY) 
		{
			DataLoader.fHumidity += Random.Range(2,7);
			if (DataLoader.fHumidity > DataLoader._OPTIM_HUMIDITY)
			{
				DataLoader.fHumidity = DataLoader._OPTIM_HUMIDITY;
			}
		}

		DataLoader.fPGrunt -= 1f;
	}

}
