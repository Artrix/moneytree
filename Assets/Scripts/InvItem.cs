﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Soomla.Store;

public class InvItem : MonoBehaviour {

	public int good_Id;
	private string goodId = "";
	private bool isLastBought;
	public Animation animationInv;
	public Animation animationItem;
	public bool isOnceUsing;

	GridLayoutGroup grid {
		get;
		set;
	}

	ScrollRect scroll {
		get;
		set;
	}


	// Use this for initialization
	void Start () {
		grid = transform.parent.GetComponent<GridLayoutGroup>();
		scroll = transform.parent.parent.GetComponent<ScrollRect> ();
	}

	private void OnEnable()
	{
		// enabled only if it is bought item
		isLastBought = false;
		goodId = PurchasedAssets.goodIds[good_Id];
		int value = -1;
		bool isValue = LocalStoreInfo.GoodsBalances.TryGetValue(goodId, out value);
		if (!isValue)
		{
			value = -1;
		}
		if (value == 0)
		{
			gameObject.SetActive(false);
		}
		else
		{
			Debug.Log(goodId);
			if (DataLoader.strLastItemId.Equals(goodId, System.StringComparison.Ordinal))
			{
				DataLoader.strLastItemId = "";
				DataLoader.invCounter++;
				isLastBought = true;
				DataLoader.isNewInventory = true;
			}
			gameObject.SetActive(true);

		}
		
	}


	// Update is called once per frame
	void Update () {
		if (isLastBought)
		{
			// is last bought item - center draggable panel on this item
			isLastBought = false;
			// Calculate the panel's center in world coordinates
			//scroll.normalizedPosition = 

		}
		int value = -1;
		bool isValue = LocalStoreInfo.GoodsBalances.TryGetValue(goodId, out value);
		if (isValue && value == 0)
		{
			//Debug.Log("disactiv " + goodId);
			gameObject.GetComponent<Animation>().Stop();
			gameObject.SetActive(false);
			//Debug.Log("grid ");

		}
	}

	public void OnEndUsing()
	{
		Debug.Log ("end using " + goodId);
		StoreInventory.TakeItem(goodId, 1);
	}

	public void OnStartUsing()
	{

		if (animationInv != null) {
			animationInv.Play();
		}
		if (animationItem != null) {
			animationItem.Play();

		}
		if (isOnceUsing) {
			StartCoroutine (DelayAnimItem ());
		}

	}

	IEnumerator DelayAnimItem(){
		if (animationItem != null) {
			yield return new WaitForSeconds (animationItem.clip.length);
			OnEndUsing ();
		} else {
			OnEndUsing ();
		}

	}
}



