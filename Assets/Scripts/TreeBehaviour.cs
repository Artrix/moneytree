using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public struct TreeFeatures
{
    string sName;
    float time;
    GameObject obj;
}

public class TreeBehaviour : MonoBehaviour
{



    public Transform prefabCoin;

	public Transform prefabCoinFriend;

    public Text labelGrowth;

	public Transform alertWindow;

    public Text alertMessage;

	public static GameObject oAlertWindow;
	
	public static Text oAlertMessage;

	// animator
	public Animator animatorTree;

    // time of speed with features
    private float timeSpeedGrowing;
    // time of money appearence
    private float fTMoney;
    //features for tree
    private ArrayList aFeatures;

    private bool isGrowthUp;
    private float prevGrowthLevel;
    private float fStepAnim;
    private const int _SCORE_IN_HOUR = 8 * 60;//* 360;
    private const int _ANIMATION_STEPS = 100;

    public static int[] levelInScores = new int[] 
	{ -2, -1, 2880, 	5760, 		9120,		12480,		16320,		20160,	  24960,		29760,		
		35520,		41280,		48000,		54720,	  62400,		70080,		78720,		87360,
		96960,		106560,		117120,		127680,		134400,
		141120,		147840,		155520,		163200,		170880,		178560,		186240,		194880,
		203520,		212160,		220800,		230400,		240000,		250560,		261120,		272640,
		284160,		295680,		307200,		318720,		330240,		342720,		355200,		367680,
		380160,		392640,		406080,		419520,		433920,		448320,		463680,		479040,
		495360,		511680,		528960,		546240,		564480,		582720,		601920,		621120,
		641280,		661440,		682560,		703680,		724800,		745920,		768000,		790080,
		812160,		834240,		856320,		879360,		902400,		925440,		948480,		971520,
		995520,		1019520,		1043520,		1067520,		1091520,		1115520,		1140480,		1165440,
		1190400,		1215360,		1241280,		1267200,		1293120,		1319040,		1345920,		1372800,
		1399680,		1426560,		1453440,		1482240,		1515840,		1550400,		1584960,		1619520,
		1654080,		1689600,		1725120,		1760640,		1796160,		1831680,		1870080};

	private int[] coinsToLevel = new int[] 
	{ 10, 		12,		14,		16,		18,		20,		22,		24,		26,		28,
		30,		32,		34,		36,		38,		40,		42,		44,		46,		48,
		51,		54,		57,		60,		63,		66,		69,		72,		75,		78,
		81,		84,		87,		90,		93,		96,		99,		102,		105,		108,
		112,		116,		120,		124,		128,		132,		136,		140,		144,		148,
		152,		156,		160,		164,		168,		172,		176,		180,		184,		188,
		194,		200,		206,		212,		218,		224,		230,		236,		242,		248,
		254,		260,		266,		272,		278,		284,		290,		296,		302,		308,
		315,		322,		329,		336,		343,		350,		357,		364,		371,		378,
		385,		392,		399,		406,		413,		420,		427,		434,		441,		448,
		448,		448,		448,		448,		448,		448,		448,		448,		448,		448};

	private int[] coinsToLevelFriend = new int[] 
	{ 2,		2,		2,		2,		3,		3,		3,		3,		3,		3,
		4,		4,		4,		4,		4,		5,		5,		5,		5,		5,
		6,		6,		6,		6,		6,		7,		7,		7,		7,		7,
		8,		8,		8,		8,		8,		9,		9,		9,		9,		9,
		10,		10,		10,		10,		10,		11,		11,		11,		11,		11,
		12,		12,		12,		12,		12,		12,		12,		12,		12,		12,
		13,		13,		13,		13,		13,		13,		13,		13,		13,		13,
		14,		14,		14,		14,		14,		14,		14,		14,		14,		14,
		15,		15,		15,		15,		15,		16,		16,		16,		16,		16,
		17,		17,		17,		17,		17,		17,		17,		17,		20,		25,
		25,		25,		25,		25,		25,		25,		25,		25,		25,		25};

    private const int MAX_LEVEL = 109;

    private const string SCORE = "tree_score";

    private const float _coinsToLevel = 0.2f;

    public const float COIN_X1 = -3.7f;
    public const float COIN_X2 = -2.0f;
    public const float COIN_X3 = 2.0f;
    public const float COIN_DX = 0.2f;
    private const float _y1 = 2.5f;
    private const float _y2 = 3.4f;

    private int[] alertLevels = { 11, 16, 36, 40 };
    private int[] potTypes = { 1, 1, 3, 3 };
    private string[] textAlerts = 
	{	"Need pot S!",
		"Buy pot S!",
		"Need pot M!",
		"Buy pot M!"};

    void Awake()
    {
        prevGrowthLevel = 0;
        isGrowthUp = false;
        fStepAnim = 0.0f;

        if (DataLoader.isDataLoaded)
        {
            updateLevel(DataLoader.iGrowth);
            animatorTree.SetFloat("Growth", (float)DataLoader.iGrowth);
			Debug.Log("growth changed 0 >"+	animatorTree.GetFloat("Growth"));
        }
		oAlertWindow = alertWindow.gameObject;
		oAlertMessage = alertMessage;
    }

    void Start()
    {
        TimeEvents.OnHourGoEnd += OnHourGo;

        
    }

    void Update()
    {
		if (DataLoader.isDataLoaded)
		{
	        if (isGrowthUp)
	        {
	            if (fStepAnim == 0.0f)
	            {
	                //Debug.Log("growth" + fStepAnim);

	                fStepAnim = (float)(DataLoader.iGrowth - prevGrowthLevel) / (float)_ANIMATION_STEPS;
	            }
	            if (prevGrowthLevel != (float)DataLoader.iGrowth)
	            {
	                if (Mathf.Abs(DataLoader.iGrowth - prevGrowthLevel) < Mathf.Abs(fStepAnim))
	                {
	                    prevGrowthLevel = (float)DataLoader.iGrowth;
	                }
	                else
	                {
	                    prevGrowthLevel += fStepAnim;
	                }
	                animatorTree.SetFloat("Growth", prevGrowthLevel);
					Debug.Log("growth changed 1 >"+	animatorTree.GetFloat("Growth"));
	            }
	            else
	            {
	                isGrowthUp = false;
	                fStepAnim = 0.0f;
	            }
	            updateLevel(DataLoader.iGrowth);

	        }
	        if (!isGrowthUp && prevGrowthLevel != 0)
	        {
	            updateLevel(DataLoader.iGrowth);
	            animatorTree.SetFloat("Growth", prevGrowthLevel);
//				Debug.Log("growth changed 2 >"+	animatorTree.GetFloat("Growth"));
	        }
	        // add coins if it needs
	        if (DataLoader.fCoinsRest >= 1)
	        {
	            AddCoinsToScene();
	        }
			if (DataLoader.fCoinsFriendRest >= 1)
			{
				AddCoinsFriendToScene();
			}
		} 
    }

    private void updateLevel(int level)
    {
        labelGrowth.text = (level + 1) + " cm";

        if (DataLoader.oUser != null)
        {
            DataLoader.oUser.SetUserLevel(level);
        }
    }

	public void SetTreeGrowth(int growth)
	{
		animatorTree.SetFloat("Growth", growth);
		isGrowthUp = false;
		prevGrowthLevel = 0;

		Debug.Log("growth changed 3 >"+	animatorTree.GetFloat("Growth"));
		updateLevel(growth);

	}

    public void OnHourGo()
    {

        float prevGLvl = (float)DataLoader.iGrowth;
        if (prevGrowthLevel == 0)
        {
            prevGrowthLevel = (float)DataLoader.iGrowth;
        }
        // check modificators
        float mod = 1f;
        if (DataLoader.fPGrunt == 0f)
        {
            mod -= 0.2f;
        }
		if ((DataLoader.timeFertilizerEnd - DateTime.UtcNow).TotalSeconds >= 0)
		{
			mod += DataLoader.fFertilizerMode;
		}
        if (DataLoader.fTemperature > DataLoader._MAX_TEMP)
        {
            mod -= 0.02f * (DataLoader.fTemperature - DataLoader._MAX_TEMP);
        }
        else if (DataLoader.fTemperature < DataLoader._MIN_TEMP)
        {
            mod -= 0.03f * (DataLoader._MIN_TEMP - DataLoader.fTemperature);
        }
        if (DataLoader.fHumidity > DataLoader._OPTIM_HUMIDITY)
        {
            mod -= 0.01f * (100f - DataLoader._OPTIM_HUMIDITY);
        }
        else if (DataLoader.fHumidity == 0f)
        {
            mod = 0f;
        }

        // no needed pot
        if ((prevGLvl >= alertLevels[1] && DataLoader.fPotType < 1f) || (prevGLvl >= alertLevels[3] && DataLoader.fPotType < 3f))
        {
            mod = 0f;
        }

        DataLoader.fScore += _SCORE_IN_HOUR * mod;

        DataLoader.fScore = Mathf.Clamp(DataLoader.fScore, 0f, (float)levelInScores[MAX_LEVEL]);

        //check level by score
        if (DataLoader.fScore >= levelInScores[MAX_LEVEL])
        {
            DataLoader.iGrowth = MAX_LEVEL;
        }
        else
        {
            for (int i = DataLoader.iGrowth; i < MAX_LEVEL - 1; i++)
            {
                if (DataLoader.fScore < levelInScores[i + 1])
                {
                    DataLoader.iGrowth = i;
                    break;
                }
            }
            for (int i = DataLoader.iGrowth; i > 0; i--)
            {
                if (DataLoader.fScore >= levelInScores[i])
                {
                    DataLoader.iGrowth = i;
                    break;
                }
            }
        }

        
        if (prevGLvl != (float)DataLoader.iGrowth)
        {
            isGrowthUp = true;
 
            
        }

		if (FB.IsLoggedIn && DataLoader.isDataLoaded)
		{
			Debug.Log("post score");
			var query = new Dictionary<string, string>();
			int score = (int)DataLoader.fScore;
			query["score"] = score.ToString();
			FB.API("/me/scores", Facebook.HttpMethod.POST, delegate(FBResult r) { Debug.Log("Result score: " + r.Text); }, query);
			DataLoader.scores[FB.UserId] = (int)DataLoader.fScore;
		}

        for (int i = 0; i < alertLevels.Length; i++)
        {

            if (DataLoader.fPotType < potTypes[i] && DataLoader.iGrowth + 1 == alertLevels[i] && DataLoader.iPotAlert < i + 1)
            {
				AlertBehaviour.oAlert.OpenAlert(0, textAlerts[i]);
                DataLoader.iPotAlert = i + 1;
            }
        }

        DataLoader.iHoursFromCoinTake++;
		DataLoader.iHoursFromCoinFriendTake++;

        AddCoins((int)prevGLvl, DataLoader.iGrowth, 1);



    }


	public static int GetGrowthFromScore(int score)
	{
		Debug.Log("getGrowth " + score);
		if (score >= levelInScores[MAX_LEVEL])
		{
			return MAX_LEVEL;
		}
		else
		{
			for (int i = 0; i < MAX_LEVEL - 1; i++)
			{
				if (score < levelInScores[i + 1])
				{
					return i;
				}
			}
		}
		return MAX_LEVEL;
	}


    public void AddCoins(int levelStart, int levelEnd, int hours)
    {
        // calculating coins count
        float coinsPerHour = 1f;
		float coinsFrPerHour = 1f;
        // check modificators
        float mod = 1f;
        if (DataLoader.fPGrunt == 0f)
        {
            mod -= 0.35f;
        }
        else if (DataLoader.fPGrunt <= 25f)
        {
            mod -= 0.2f;
        }
        else if (DataLoader.fPGrunt <= 50f)
        {
            mod -= 0.1f;
        }
        if (DataLoader.fTemperature > DataLoader._MAX_TEMP)
        {
            mod -= (DataLoader._MAX_TEMP - DataLoader.fTemperature) * 0.03f;
        }
        else if (DataLoader.fTemperature > DataLoader._MIN_TEMP)
        {
            mod -= (DataLoader.fTemperature - DataLoader._MIN_TEMP) * 0.03f;
        }
        if (DataLoader.fHumidity == 0f)
        {
            mod -= 0.3f;
        }
		float mod1 = mod;
		float mod2 = mod;
        if (DataLoader.iHoursFromCoinTake > 48)
        {
            mod1 = 0f;
        }
		if (DataLoader.iHoursFromCoinFriendTake > 48)
		{
			mod2 = 0f;
		}
        if (levelStart - levelEnd < 2)
        {
            coinsPerHour = (float)coinsToLevel[levelEnd] / (float)DataLoader._COST_COIN;
			coinsFrPerHour = ((float)coinsToLevelFriend[levelEnd] / 24f) / (float)DataLoader._COST_COIN_FRIEND ;
        }
        else
        {
			int level = (int)Mathf.Round((levelStart + levelEnd) * 0.5f);
			coinsPerHour = (float)coinsToLevel[level] / (float)DataLoader._COST_COIN;
			coinsFrPerHour = ((float)coinsToLevelFriend[level] / 24f) / (float)DataLoader._COST_COIN_FRIEND ;
        }
        DataLoader.fCoinsRest += hours * coinsPerHour * mod1;
		DataLoader.fCoinsFriendRest += hours * coinsFrPerHour * mod2;


    }

    public void AddCoinsToScene()
    {
        int coins = Mathf.RoundToInt(DataLoader.fCoinsRest);
        DataLoader.fCoinsRest -= coins;
        /// test cons
        //coins = 1;

        Vector3 position = Vector3.zero;
        for (int i = 0; i < coins; ++i)
        {
            float lineSelector = UnityEngine.Random.value;
            if (lineSelector < 0.5f)
            {
                position.x = UnityEngine.Random.Range(COIN_X1 - COIN_DX, COIN_X1 + COIN_DX);
            }
            else 
            {
                position.x = UnityEngine.Random.Range(COIN_X2 - COIN_DX, COIN_X2 + COIN_DX);
            }
            position.y = UnityEngine.Random.Range(_y1 + i * COIN_DX, _y1 + (i + 1) * COIN_DX);
            //					position.z = prefabCoin.transform.position.z -(i+countsCoinsExist+iMoney+1)*0.0001f;
            DataLoader.minZCoin -= 0.000001f;
            position.z = DataLoader.minZCoin;

			Transform coinObj = Instantiate(prefabCoin, position, Quaternion.identity) as Transform;

			if (coinObj != null && DataLoader.oParentCoins != null)
			{
				coinObj.parent = DataLoader.oParentCoins;
				coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN;
			}
        }
    }

	public void AddCoinsFriendToScene()
	{
		int coins = Mathf.RoundToInt(DataLoader.fCoinsFriendRest);
		DataLoader.fCoinsFriendRest -= coins;
		/// test cons
		//coins = 1;
		
		Vector3 position = Vector3.zero;
		for (int i = 0; i < coins; ++i)
		{
			position.x = UnityEngine.Random.Range(COIN_X3 - COIN_DX, COIN_X3 + COIN_DX);
			position.y = UnityEngine.Random.Range(_y1 + i * COIN_DX, _y1 + (i + 1) * COIN_DX);
			//					position.z = prefabCoin.transform.position.z -(i+countsCoinsExist+iMoney+1)*0.0001f;
			DataLoader.minZCoin -= 0.000001f;
			position.z = DataLoader.minZCoin;
			
			Transform coinObj = Instantiate(prefabCoinFriend, position, Quaternion.identity) as Transform;
			
			if (coinObj != null && DataLoader.oParentCoins != null)
			{
				coinObj.parent = DataLoader.oParentCoins;
				coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN_FRIEND;
			}
		}
	}

	public void OnShovelClick()
	{
		DataLoader.fPGrunt += 50;
	}

	public void OnFertilizerClick(int days)
	{
		switch (DataLoader.iSpeed) {
		case 1:
			DataLoader.timeFertilizerEnd = DateTime.UtcNow.AddDays(days);
			break;
		case 2:
			DataLoader.timeFertilizerEnd = DateTime.UtcNow.AddMinutes(days * 24);
			break;
		case 3:
			DataLoader.timeFertilizerEnd = DateTime.UtcNow.AddSeconds(days * 24);
			break;
		}

	}

	public void SetFertilizerMode(int mod)
	{
		DataLoader.fFertilizerMode = mod * 0.01f;
	}


}
