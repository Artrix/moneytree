using UnityEngine;
using System.Collections;
using Soomla.Store;

public class CoinScript : MonoBehaviour
{

    public float fallSpeed = 4.0f;
	public int costCoin = 100;

    private Transform groundCheck;

    // Use this for initialization
    void Start()
    {

    }

    void Awake()
    {
        groundCheck = transform.Find("GroundChecker").transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody2D>().isKinematic = false;

        Collider2D[] groundHits = Physics2D.OverlapPointAll(groundCheck.position, 1);
        foreach (Collider2D c in groundHits)
        {
            if (c.gameObject.tag == "coin"
                || c.gameObject.tag == "windowFrame")
            {
                gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                break;
            }
        }
    }

	public void TakeCoin()
	{
		DataLoader.iMoneyCoins += costCoin;
		StoreInventory.GiveItem (PurchasedAssets.COINS_ITEM_ID, costCoin);
		Destroy (gameObject, 0);
	}
}
