using UnityEngine;
using System.Collections;
using System;
using Soomla.Store;
using UnityEngine.UI;

public class BuyInMarket : MonoBehaviour
{
    private string goodId;

    public int good_Id;
    public Text labelGoodDescr;
	public Text labelGoodDescrFull;
	public Text labelCoins;
	public Text labelRubies;
	public Image imageGood;


    void Start()
    {
        StoreEvents.OnItemPurchased += OnItemPurchased;
    }


    void OnDisable()
    {
        //		StoreEvents.OnMarketPurchase += OnMarketPurchase;
        //	StoreEvents.OnItemPurchased -= OnItemPurchased;
        //	StoreEvents.OnUnexpectedErrorInStore -= OnUnexpectedErrorInStore;
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void LoadGood(int gId, string text, Sprite image, string coins, string rubies)
    {
        //Debug.Log (gId);
        if (gId < PurchasedAssets.goodIds.Length)
        {
            goodId = PurchasedAssets.goodIds[gId];
            //Debug.Log (" ------- load " + gId + " " + goodId);

            VirtualItem item = StoreInfo.GetItemByItemId(goodId);

			if (labelGoodDescrFull != null && item != null)
			{
				labelGoodDescrFull.text = item.Description;
			}

            if (labelGoodDescr != null)
            {
                labelGoodDescr.text = text;
            }

			if (imageGood != null)
			{
				imageGood.sprite = image;
			}

			if (labelCoins != null)
			{
				labelCoins.text = coins;
			}

			if (labelRubies != null)
			{
				labelRubies.text = rubies;
			}
        }

    }

    void OnGUI()
    {

    }


    public void OnClick()
    {
        if (good_Id < PurchasedAssets.goodIds.Length && good_Id > 0)
        {
            goodId = PurchasedAssets.goodIds[good_Id];
        }
        if (goodId != null)
        {
            try
            {
                //Debug.Log (" ------- " + good_Id + " " + goodId);
                //		result += ("Buy " + good.ItemId + " > ");
                StoreInventory.BuyItem(goodId);
            }
            catch (Exception e)
            {
                if (DataLoader.spinner != null)
                {
                    DataLoader.spinner.SetActive(true);
                }
                Debug.Log("Exeption > " + e.Message);
            }
        }

    }

    public void OnItemPurchased(PurchasableVirtualItem pvi)
    {
        if (StoreInfo.GetCategoryForVirtualGood(pvi.ItemId).Name.Equals(PurchasedAssets.INVENTORY_CATEGORY.Name, StringComparison.Ordinal))
        {
            DataLoader.strLastItemId = pvi.ItemId;
        };
    }



}
