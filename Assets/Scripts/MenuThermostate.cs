using UnityEngine;
using System.Collections;

public class MenuThermostate : MonoBehaviour {

	private Animator anim;

	const int _MAX_REGUL = 3;
	const int _MIN_REGUL = 0;

	private int tempThermostate;
	// Use this for initialization
	void Start () {
		tempThermostate  = DataLoader.iThermostate * 4 + MenuThermometr._aiTempDef[System.DateTime.Now.Month - 1];
		anim = gameObject.GetComponent<Animator> ();
		anim.SetFloat ("Regul", (float)DataLoader.iThermostate);
		TimeEvents.OnHourGo += OnHourGo;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick()
	{
		float regul = anim.GetFloat("Regul") + 1;
		if (regul > _MAX_REGUL) {
						regul = 0;
				}
		anim.SetFloat("Regul", regul);
		DataLoader.iThermostate = (int)regul;
		tempThermostate  = DataLoader.iThermostate * 4 + MenuThermometr._aiTempDef[System.DateTime.Now.Month - 1];
	//	TimeEvents.OnMinutesGo -= OnMinutesGo;
	}

	public void OnHourGo()
	{

		if (DataLoader.fTemperature < tempThermostate) {
						DataLoader.fTemperature += 1f;
						if (DataLoader.fTemperature > tempThermostate)
								DataLoader.fTemperature = tempThermostate;
//				} else {
//					TimeEvents.OnMinutesGo -= OnMinutesGo;
			}

	}
}
