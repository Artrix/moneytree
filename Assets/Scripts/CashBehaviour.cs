using UnityEngine;
using System.Collections;
using Soomla.Store;
using UnityEngine.UI;

public class CashBehaviour : MonoBehaviour {

	public static bool isStoreInit = false;

	public Transform alertWindow;
	public Text alertMessage;

	private static CashBehaviour instance = null;

	void Awake() {
		if(instance == null){  //making sure we only initialize one instance.
			instance = this;
			GameObject.DontDestroyOnLoad(this.gameObject);
		} else {     //Destroying unused instances.
			GameObject.Destroy(this);
		}
	}

	// Use this for initialization
	void Start () {
		SoomlaStore.Initialize (new PurchasedAssets ());

		StoreEvents.OnMarketPurchaseStarted += OnMarketPurchaseStarted;
		StoreEvents.OnMarketPurchase += OnMarketPurchase;
		StoreEvents.OnItemPurchaseStarted += OnItemPurchaseStarted;
		StoreEvents.OnItemPurchased += OnItemPurchased;
		StoreEvents.OnSoomlaStoreInitialized += OnStoreControllerInitialized;
		StoreEvents.OnUnexpectedErrorInStore += OnUnexpectedErrorInStore;
		StoreEvents.OnCurrencyBalanceChanged += OnCurrencyBalanceChanged;
		StoreEvents.OnGoodBalanceChanged += OnGoodBalanceChanged;
		StoreEvents.OnMarketPurchaseCancelled += OnMarketPurchaseCancelled;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	string result = "start > ";
	
	
	public void OnClick()
	{
//		StoreInventory.BuyItem (PurchasedAssets.HUNDRED_COINS_PACK.ItemId);
//		result += "try to buy > ";
	}
	
	
	void OnGUI()
	{
//		GUILayout.Label (result);
	}
	
	public void OnMarketPurchaseStarted(PurchasableVirtualItem pvi)
	{
		result += " market purchase started " + pvi.ItemId +" > ";
		
	}
	
	public void OnMarketPurchase(PurchasableVirtualItem pvi, string purchaseToken, string other)
	{
		result += " market purchase " + pvi.ItemId +" with token " + purchaseToken+ " > ";
		
	}
	
	public void OnItemPurchaseStarted (PurchasableVirtualItem pvi)
	{
		result += "item purchase started " + pvi.ItemId +" > ";
		if (DataLoader.spinner != null)
			DataLoader.spinner.SetActive (true);
	}
	
	public void OnItemPurchased(PurchasableVirtualItem pvi)
	{
		result += "item purchased " + pvi.ItemId +" > ";
//		DataLoader.iMoney = StoreInventory.GetItemBalance (PurchasedAssets.COINS_ITEM_ID);
		if (DataLoader.spinner != null)
			DataLoader.spinner.SetActive (false);

	}

	public void OnMarketPurchaseCancelled(PurchasableVirtualItem pvi)
	{
		result += "item purchase canceled " + pvi.ItemId +" > ";
		//		DataLoader.iMoney = StoreInventory.GetItemBalance (PurchasedAssets.COINS_ITEM_ID);
		if (DataLoader.spinner != null)
			DataLoader.spinner.SetActive (false);
		
	}
	
	public void OnStoreControllerInitialized()
	{
		isStoreInit = true;
		LocalStoreInfo.Init();
		StoreInventory.BuyItem (PurchasedAssets.TEST_ITEM_ID);
		result += " store controller init "  +" > ";
        //Debug.Log ("store controller init");
		
	}
	
	public void OnUnexpectedErrorInStore(string error)
	{
		result += " error " + error +" > ";
		if (DataLoader.spinner != null)
			DataLoader.spinner.SetActive (false);
        //Debug.Log ("___OnUnexpectedErrorInStore: " + error);
		alertMessage.text = error;
		if (error.Equals ("")) {
			alertMessage.text = "iTunes Connect is unavailable.";
				}
		alertWindow.gameObject.SetActive (true);
		
	}

	public void OnCurrencyBalanceChanged(VirtualCurrency vc, int balance, int amountAdded)
	{
		LocalStoreInfo.UpdateBalances();
//		PlayerPrefs.SetInt ("money", DataLoader.iMoney);
	}

	public void OnGoodBalanceChanged(VirtualGood good, int balance, int amountAdded) {
		LocalStoreInfo.UpdateBalances();
//		PlayerPrefs.SetInt ("money", DataLoader.iMoney);
	}
	
}