using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class MoneyBehaviour : MonoBehaviour {


	void Awake()
	{

	}
	
	void Update()
	{

	}
	
	public void OnClick()
	{

		DataLoader.iHoursFromCoinTake = 0;
		TakeCoins ();

	}

	public void OnClickFriend()
	{
		// take friend coin or no
		if (DataLoader.userViewId.CompareTo(FB.UserId) != 0)
		{

			DataLoader.iHoursFromCoinFriendTake = 0;
			TakeCoins ();

		} else{
			AlertBehaviour.oAlert.OpenAlert( 0, "It's friend coin");
		}
		
	}

	private void TakeCoins()
	{
		GameObject[] coins = GameObject.FindGameObjectsWithTag ("coin");
		foreach(GameObject coin in coins)
		{
			if(Mathf.Abs(coin.transform.position.x - transform.position.x) <= TreeBehaviour.COIN_DX * 2)
			{
				coin.GetComponent<CoinScript>().TakeCoin();
			}
		}
	}

}