﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class _TimeButton : MonoBehaviour {

	public Text label;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		switch (DataLoader.iSpeed)
		{
		case 1: 
			label.text = "H";
			break;
		case 2:
			label.text = "M";
			break;
		case 3:
			label.text = "S";
			break;
		}
	}

	public void OnClick()
	{
		DataLoader.iSpeed += 1;
		if (DataLoader.iSpeed > 3) {
						DataLoader.iSpeed = 1;
				}
	}
}
