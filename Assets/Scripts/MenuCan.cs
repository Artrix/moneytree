using UnityEngine;
using System.Collections;

public class MenuCan : MonoBehaviour {

	public bool isWatering;
	// animator
	private Animator animatorMenu;

	public float fWatered;
	private bool isWatered;
	
	void Awake()
	{
		animatorMenu = this.GetComponent<Animator> ();
		isWatering = false;
		isWatered = false;
		fWatered = 0.0f;
	}
	
	void Update()
	{
		if (isWatered && fWatered == 0.0f)
						isWatered = false;
		if (fWatered > 0.9 && !isWatered ) {
            //Debug.Log("is Watered" +DataLoader.fHumidity);
			fWatered = 0.0f;
			isWatered = true;
			if (DataLoader.fHumidity < DataLoader._OPTIM_HUMIDITY)
			{
				DataLoader.fHumidity = DataLoader._OPTIM_HUMIDITY;
			} else {
				DataLoader.fHumidity = DataLoader._maxHumidity;
			}

			animatorMenu.SetBool ("Watering", false);
		}
	}
	
	public void OnClick()
	{

		if (!isWatering) {

			animatorMenu.SetBool ("Watering", true);
			isWatering = true;
		} else {
			fWatered = 0.0f;
			isWatering = false;
			animatorMenu.SetBool ("Watering", false);

		}
	}
	
}
