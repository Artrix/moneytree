using UnityEngine;
using System.Collections;
using Soomla.Store;
using System.Collections.Generic;

public static class LocalStoreInfo {

	// In this example we have a single currency so we can just save its balance. 
	// If you have more than one currency then you'll have to save a dictionary here.
		
	public static Dictionary<string, int> GoodsBalances = new Dictionary<string, int>();
	public static List<VirtualCurrency> VirtualCurrencies = null;
	public static List<VirtualGood> VirtualGoods = null;
	public static List<VirtualCurrencyPack> VirtualCurrencyPacks = null;
	
	public static void UpdateBalances() {
		if (VirtualCurrencies.Count > 0) {
			DataLoader.iMoneyCoins = StoreInventory.GetItemBalance(VirtualCurrencies[0].ItemId);
			DataLoader.iMoneyRubies = StoreInventory.GetItemBalance(VirtualCurrencies[1].ItemId);
		}
		foreach(VirtualGood vg in VirtualGoods){
			GoodsBalances[vg.ItemId] = StoreInventory.GetItemBalance(vg.ItemId);
		}
	}

	public static void ResetBalances() {
		if (VirtualCurrencies != null) {
						if (VirtualCurrencies.Count > 0) {
								DataLoader.iMoneyCoins = StoreInventory.GetItemBalance (VirtualCurrencies [0].ItemId);
								DataLoader.iMoneyRubies = StoreInventory.GetItemBalance (VirtualCurrencies [1].ItemId);
								StoreInventory.TakeItem (VirtualCurrencies [0].ItemId, DataLoader.iMoneyCoins);
								StoreInventory.TakeItem (VirtualCurrencies [1].ItemId, DataLoader.iMoneyRubies);
						}
						foreach (VirtualGood vg in VirtualGoods) {
								GoodsBalances [vg.ItemId] = StoreInventory.GetItemBalance (vg.ItemId);
								StoreInventory.TakeItem (vg.ItemId, GoodsBalances [vg.ItemId]);
						}
						UpdateBalances ();
				}
	}
	
	public static void Init() {
		VirtualCurrencies = StoreInfo.GetVirtualCurrencies();
		VirtualGoods = StoreInfo.GetVirtualGoods();
		VirtualCurrencyPacks = StoreInfo.GetVirtualCurrencyPacks(); 
		// test give currencies

		UpdateBalances();
	}

	public static void TestTakeCurrency()
	{
		StoreInventory.GiveItem (VirtualCurrencies[0].ItemId, 10000);
	}
}