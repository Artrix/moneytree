 using UnityEngine;
using System.Collections;

public class AttachedText : MonoBehaviour {

	public GameObject guiTextObject;
	public float offsetX;
	public float offsetY;

	// Use this for initialization
	void OnGUI () {
		if (guiTextObject)
		{
			guiTextObject.GetComponent<GUIText>().color = DataLoader.colorText;

			Vector3 position = gameObject.transform.position;
			position.x += offsetX;
			position.y += offsetY;
			Vector3 tmpPos = Vector3.zero;
			tmpPos.z = guiTextObject.transform.position.z;
			guiTextObject.transform.position = tmpPos;

			position = Camera.main.WorldToScreenPoint (position);
			guiTextObject.GetComponent<GUIText>().pixelOffset = position;
		
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
