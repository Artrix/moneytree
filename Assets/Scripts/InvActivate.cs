﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InvActivate : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Activate()
	{
		Transform myTrans = transform;
		
		DataLoader.invCounter = 0;
		DataLoader.isNewInventory = false;
		for (int i = 0; i < myTrans.childCount; ++i) {
			Transform t = myTrans.GetChild (i);
			t.gameObject.SetActive(true);
			t.gameObject.GetComponent<Button>().targetGraphic.rectTransform.localScale = new Vector3(1f, 1f, 1f);
		}
		//Debug.Log("___________reposition grid");
	//	myTrans.gameObject.GetComponent<GridLayoutGroup> ().;
		//Debug.Log("___________reposition grid end");
	}
}
