using UnityEngine;
using System.Collections;
using System;



public class TimeEvents : MonoBehaviour
{
    int prevHour;

    const int _DAY_HOUR = 6;
    const int _NIGHT_HOUR = 21;
    // Use this for initialization
    void Start()
    {
        prevHour = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (DataLoader.isDataLoaded)
        {
            TimeUpdate();
        }
    }

    void TimeUpdate()
    {
        DateTime now = DateTime.UtcNow;
        int hour = (int)(DateTime.Now - DateTime.Today).TotalHours;
        int hours = 0;
        switch (DataLoader.iSpeed)
        {
            case 1:
                hours = (int)(now - DataLoader.timeChecked).TotalHours;
                break;
            case 2:
                hours = (int)(now - DataLoader.timeChecked).TotalMinutes;
                break;
            case 3:
                hours = (int)(now - DataLoader.timeChecked).TotalSeconds;
                break;
        }

        //	int hours = (int)(now - DataLoader.timeChecked).TotalHours;


        if (hour != prevHour)
        {
            TimeEvents.OnHourChanged(hour);
            prevHour = hour;
            //			Debug.Log("hour "+hour);
            if (hour == _DAY_HOUR)
            {
                TimeEvents.OnDayBegan();
            }
            else if (hour == _NIGHT_HOUR)
            {
                TimeEvents.OnNightBegan();
            }
        }
        //		if (minutes != 0) 
        //		{
        //			DataLoader.timeChecked = now;
        //			int minutesChecked = 60;
        //			for (;minutes > 0;)
        //			{
        //				if (minutes < minutesChecked)
        //				{
        //					minutesChecked = minutes;
        //				}
        //				minutesForHour += minutesChecked;
        //				minutes -= minutesChecked;
        //				TimeEvents.OnMinutesGoStart (minutesChecked);
        //				TimeEvents.OnMinutesGo (minutesChecked);
        //				TimeEvents.OnMinutesGoEnd (minutesChecked);
        //				if (minutesForHour >= 60)
        //				{
        //					minutesForHour -= 60;
        //					TimeEvents.OnHourGoStart (1);
        //					TimeEvents.OnHourGo (1);
        //					TimeEvents.OnHourGoEnd (1);
        //				}
        //			}
        //		}
        if (hours > 0)
        {
            DataLoader.timeChecked = now;
            for (; hours > 0; hours--)
            {
                TimeEvents.OnHourGoStart();
                TimeEvents.OnHourGo();
                TimeEvents.OnHourGoEnd();
            }
        }


    }


    public delegate void Action();

    public static Action<int> OnHourChanged = delegate { };

    public static Action OnDayBegan = delegate { };

    public static Action OnNightBegan = delegate { };

    public static Action<int> OnMinutesGoStart = delegate { };

    public static Action<int> OnMinutesGo = delegate { };

    public static Action<int> OnMinutesGoEnd = delegate { };

    public static Action OnHourGoStart = delegate { };

    public static Action OnHourGo = delegate { };

    public static Action OnHourGoEnd = delegate { };

	public static Action OnDataLoaded = delegate { };

	public static Action<int> OnDialogResult = delegate { };







}
