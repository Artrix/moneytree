using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuThermometr : MonoBehaviour {

	public Transform indThemp;
	public Transform indHumidity;
	public Text labelTemp;
	public Text labelHumidity;
	public string simbolTemperature;

	const int _MIN_NORM_THEMP = 18;
	const int _MAX_NORM_THEMP = 25;

	static public int[] _aiTempDef = new int[]
	{10, 8, 12, 17, 22, 27, 35, 30, 24, 18, 15, 11};

	static public int[] _aiTempWin = new int[]
	{8, 6, 10, 15, 20, 22, 25, 24, 20, 16, 13, 9};
	// Use this for initialization
	void Start () {
		UpdateParams ();
		TimeEvents.OnHourGoStart += OnHourGo;
	}


	// Update is called once per frame
	void Update () {
		UpdateParams ();
	}

	void UpdateParams()
	{
		int themp = Mathf.RoundToInt( DataLoader.fTemperature);
		labelTemp.text = themp + " " + simbolTemperature;
		indThemp.gameObject.SetActive (!DataLoader.isThempNormal (themp));
		labelHumidity.text = (int)DataLoader.fHumidity + " %";
		indHumidity.gameObject.SetActive (!DataLoader.isHumidityNormal ());
		
	}

	public void OnHourGo()
	{
				int defTemp = MenuThermometr._aiTempDef [System.DateTime.Now.Month - 1];
				if (DataLoader.fTemperature > defTemp) {
						DataLoader.fTemperature -= 0.1f;
						if (DataLoader.fTemperature < defTemp)
								DataLoader.fTemperature = defTemp;

				} else if (DataLoader.fTemperature < defTemp) {
						DataLoader.fTemperature += 1f/12f;
						if (DataLoader.fTemperature > defTemp)
								DataLoader.fTemperature = defTemp;
			
				}
	}
}
