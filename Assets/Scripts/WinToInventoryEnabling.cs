using UnityEngine;
using System.Collections;
using Soomla.Store;


public class WinToInventoryEnabling : MonoBehaviour {

	public Transform InventoryObject;
	public InvActivate InventoryGridActivator;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnClick() {
		if (DataLoader.strLastItemId.Length != 0){
			//Debug.Log("? _________" + DataLoader.strLastItemId);
			InventoryObject.gameObject.SetActive(true);
			InventoryGridActivator.Activate();
		}
	}
}
