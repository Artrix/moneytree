using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Facebook.MiniJSON;
using System;
using Soomla;
using UnityEngine.UI;


public class DataLoader : MonoBehaviour
{

	public Text textError;

    public Transform scene;

    public Transform prefabCoin;

	public Transform prefabFriendCoin;

	public Transform parentCoins;

    public Transform prefabFriend;

    public RectTransform gridFriends;

    public Transform tSpinner;

    public Transform tInventory;

	public TreeBehaviour oTree;

    //	public RawImage  myPicture;

    public static bool isDataLoaded;

	public static DataLoader data;

	public static string userViewId;

    public static float minZCoin;

    public static FriendBehaviour oUser;


    // count of money
    static public int iMoneyCoins;
	// count of money
	static public int iMoneyRubies;

    //humidity
    static private float humidity;
    static public float fHumidity
    {
        get { return humidity; }
        set
        {
            humidity = value;
            if (humidity < 0)
            {
                humidity = 0f;
            }
            else if (humidity > 100)
            {
                humidity = 100f;
            }
        }
    }
    //next time of iHumidity changing
    static public DateTime timeHumidity;
    // current growth of tree
    static public int iGrowth;
    // for color animation
    public static Color colorText;
    // speed with features
    static public float fCurSpeedGrowth;

    static public float fTemperature;

    static private float pgrunt;
    static public float fPGrunt
    {
        get { return pgrunt; }
        set
        {
            pgrunt = value;
            if (pgrunt < 0)
            {
                pgrunt = 0f;
            }
            else if (pgrunt > 100)
            {
                pgrunt = 100f;
            }
        }
    }

    static public int iThermostate;

    static public float fScore;

    static public int iWindowClosed;

    static public int iSoundOff;

    static public DateTime timeChecked;

    static public float fWallType;

    static public float fPotType;

    static public int iPotAlert;

    static public float fCoinsRest;

	static public float fCoinsFriendRest;
    // item from inventory is bought last
    static public string strLastItemId;
    // counter of inventory items
    static public int invCounter;

    static public bool isNewInventory;

    static public GameObject oInventory;

    static public GameObject spinner;

    public static DateTime timeAutoWaterEnd;

    //	public static bool isAutoWater;

    public static int iAutoWaterDays;

    public static int iHoursFromCoinTake;

	public static int iHoursFromCoinFriendTake;

	public static DateTime timeFertilizerEnd;
	
	//	public static bool isAutoWater;
	
	public static float fFertilizerMode;
	
	public static int iSpeed;

	public static Transform oParentCoins;

    // consts -----------------------

	public const int _AVATAR_WIDTH = 128;
	public const int _AVATAR_HEIGHT = 128;

    public const int _dayInHours = 24;

    public const int _minHumidity = 0;

    public const int _maxHumidity = 100;

    public const int _OPTIM_HUMIDITY = 75;
    public const float _DOWN_HUMIDITY_MINUTE = 0.0417f;

    public const int _MIN_TEMP = 18;
    public const int _MAX_TEMP = 25;


    public const int _lostDayHumidity = 5;
    // speed of growing
    public const float _fSpeedGrowth = 1f;

    public const float _fCoinsPerGrowth = 0.5f;

    public const int _iMinThemp = 6;

	public const int _COST_COIN = 100;
	public const int _COST_COIN_FRIEND = 1;


    private struct transformStruct
    {
        public Vector3 position;
        public Quaternion rotation;
    }

    private static List<object> friends = null;
    private static Dictionary<string, string> profile = null;
    private static Dictionary<string, string> profileFriends = new Dictionary<string, string>();
    public static Dictionary<string, int> scores = new Dictionary<string, int>();
    public static Dictionary<string, Texture> friendImages = new Dictionary<string, Texture>();

    //private string lastResponse = "";

    // Use this for initialization
    void Awake()
    {
		data = this;
        scene.gameObject.SetActive(false);

        isDataLoaded = false;
        //		FB.Init(OnInitComplete, OnHideUnity);
        enabled = false;

        colorText = Color.black;

        strLastItemId = "";
        //local load
        //		LocalLoadData ();

        spinner = tSpinner.gameObject;
        oInventory = tInventory.gameObject;
		oParentCoins = parentCoins;

        minZCoin = prefabCoin.transform.position.z;

        FB.Init(OnInitComplete, OnHideUnity);


    }

    void Start()
    {
        //	StoreController.Initialize (new PurchasedAssets ());


    }

    private void OnInitComplete()
    {
        Debug.Log("FB.Init completed");
        enabled = true;
        if (FB.IsLoggedIn)
        {
            Debug.Log("Already logged in");
            OnLoggedIn();
        }
        else
        {
			FB.Login("public_profile, user_photos, basic_info, email, publish_actions, user_friends", LoginCallback);
        }



    }

    private void OnHideUnity(bool isGameShown)
    {
        //Debug.Log("Is game showing? " + isGameShown);
        if (!isGameShown)
        {
            // pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // start the game back up - we're getting focus again
            Time.timeScale = 1;
        }

    }

    void LoginCallback(FBResult result)
    {
        if (result.Error != null)
        {
            //lastResponse = "Error Response:\n" + result.Error;
        }
        else if (!FB.IsLoggedIn)
        {
            //lastResponse = "Login cancelled by Player";
        }
        else
        {
            //lastResponse = "Login was successful!";
            OnLoggedIn();
        }
    }

    //	string meQueryString = "/me?fields=id,first_name,friends.limit(100).fields(first_name,id,picture.width(128).height(128)),invitable_friends.limit(100).fields(first_name,id,picture.width(128).height(128))";
    string meQueryString = "/me?fields=id,first_name,friends.fields(first_name,id,picture.width(128).height(128))";

    void OnLoggedIn()
    {
        Debug.Log("Logged in. ID:12" +
            "" + FB.UserId);
		userViewId = FB.UserId;
        // Reqest player info and profile picture
        FB.API(meQueryString, Facebook.HttpMethod.GET, APICallback);
		LoadPicture(Util.GetPictureURL("me", _AVATAR_WIDTH, _AVATAR_HEIGHT), _AVATAR_WIDTH, _AVATAR_HEIGHT, MyPictureCallback);


    }

    private void QueryScores()
    {
		Debug.Log("scoresLoad");
        FB.API("/app/scores?fields=score,user", Facebook.HttpMethod.GET, ScoresCallback);
    }

    void APICallback(FBResult result)
    {
        Debug.Log("APICallback");
        if (result.Error != null)
        {
            Util.LogError(result.Error);
            // Let's just try again
            FB.API(meQueryString, Facebook.HttpMethod.GET, APICallback);
            return;
        }

        profile = Util.DeserializeJSONProfile(result.Text);
        Debug.Log(result.Text);
        profileFriends.Add(FB.UserId, (string)profile["first_name"]);
        //		GameStateManager.Username = profile["first_name"];
        friends = Util.DeserializeJSONFriends(result.Text);
        QueryScores();

    }

    void MyPictureCallback(Texture texture)
    {
        Debug.Log("MyPictureCallback");

        if (texture == null)
        {
            // Let's just try again
			LoadPicture(Util.GetPictureURL("me", _AVATAR_WIDTH, _AVATAR_HEIGHT), _AVATAR_WIDTH, _AVATAR_HEIGHT, MyPictureCallback);

            return;
        }
        friendImages.Add(FB.UserId, texture);
        //		myPicture.texture = texture;
        //		myPicture.enabled = true;
        //		GameStateManager.UserTexture = texture;
    }

    private int getScoreFromEntry(object obj)
    {
        Dictionary<string, object> entry = (Dictionary<string, object>)obj;
        return Convert.ToInt32(entry["score"]);
    }

    void ScoresCallback(FBResult result)
    {
        Debug.Log("ScoresCallback");
        if (result.Error != null)
        {
            Util.LogError(result.Error);
            return;
        }

        //		scores = new List<object>();
        List<object> scoresList = Util.DeserializeScores(result.Text);

        scoresList.Sort(delegate(object firstObj, object secondObj)
        {
            return -getScoreFromEntry(firstObj).CompareTo(getScoreFromEntry(secondObj));
        }
                );
        foreach (object score in scoresList)
        {
            var entry = (Dictionary<string, object>)score;
            var user = (Dictionary<string, object>)entry["user"];

            string userId = (string)user["id"];

            int playerScore = getScoreFromEntry(entry);

            scores.Add(userId, playerScore);

        }
        // Now sort the entries based on score
		Debug.Log("addFriends");
        AddFriendsToScene();
        LocalLoadData();

    }



    void OnApplicationFocus(bool hasFocus)
    {
        Util.Log("hasFocus " + (hasFocus ? "Y" : "N"));
    }






    void Update()
    {

    }


    public static bool isHumidityNormal()
    {
        return (fHumidity > _minHumidity && fHumidity < _maxHumidity);
    }

    public static bool isThempNormal(int themp)
    {
        return (themp >= _MIN_TEMP && themp <= _MAX_TEMP);
    }


    void AddFriendsToScene()
    {
        Debug.Log("friends");
        if (friends != null && friends.Count > 0)
        {

            Debug.Log("friends ok");
            foreach (Dictionary<string, object> friend in friends)
            {
                profileFriends.Add((string)friend["id"], (string)friend["first_name"]);
            }
        }
        //				if (!friendImages.ContainsKey(userId)) && friend["installed"].Equals("false"))
        foreach (KeyValuePair<string, int> score in scores)
        {
            GameObject fr = Instantiate(prefabFriend.gameObject) as GameObject;
            Debug.Log("fr " + (fr == null));
            if (fr != null)
            {
                Debug.Log("create fr " + (fr == null));
                fr.transform.SetParent(gridFriends.transform);
                fr.transform.localScale = new Vector3(1f, 1f, 1f);
                fr.transform.position = prefabFriend.position;
                FriendBehaviour frScr = fr.GetComponent<FriendBehaviour>();
                frScr.data = this;
                frScr.SetUserId(score.Key);
                frScr.SetUserName(profileFriends[score.Key]);
                frScr.SetUserLevel(TreeBehaviour.GetGrowthFromScore(score.Value));
                if (FB.UserId == score.Key)
                {
                    oUser = frScr;
                }

            }
        }
        //			foreach(KeyValuePair<string, string> prof in profileFriends)
        //				{
        //					Debug.Log("friends "+prof.Key);
        //					GameObject fr = Instantiate(prefabFriend.gameObject) as GameObject;
        //					Debug.Log("fr "+(fr==null));
        //					if (fr != null)
        //					{
        //						Debug.Log("create fr "+(fr==null));
        //						fr.transform.SetParent(gridFriends.transform);
        //						fr.transform.localScale = new Vector3(1f,1f,1f);
        //						fr.transform.position = prefabFriend.position;
        //						FriendBehaviour frScr = fr.GetComponent<FriendBehaviour>();
        //						frScr.data = this;
        //						frScr.SetUserId(prof.Key);
        //						frScr.SetUserName(prof.Value);
        //					}
        //				}

        gridFriends.sizeDelta = new Vector2(gridFriends.sizeDelta.x, gridFriends.sizeDelta.y * (friends.Count + 1));
        //			gridFriends.transform.localPosition = new Vector3(0f, - (gridFriends.sizeDelta.y * 0.5f), 0f);


        //		LoadPicture(Util.GetPictureURL((string)friend["id"], 128, 128),FriendPictureCallback);

    }

    public void FriendPictureCallback(Texture texture)
    {
        //		GameStateManager.FriendTexture = texture;
    }

    public delegate void LoadPictureCallback(Texture texture);


    IEnumerator LoadPictureEnumerator(string url, int width, int height, LoadPictureCallback callback)
    {
		url = url.Replace("\\/", "/");
        WWW www = new WWW(url);
		yield return www;
		if (www.error != null) {
			textError.text += " > " + www.error + " > ";
			callback (null);
		} else {
			callback (www.texture);
		}
    }

	public void LoadPicture(string url, int width, int height, LoadPictureCallback callback)
    {
        FB.API(url, Facebook.HttpMethod.GET, result =>
               {
                   if (result.Error != null)
                   {
                       Util.LogError(result.Error);
                       return;
                   }

                   var imageUrl = Util.DeserializePictureURLString(result.Text);

			StartCoroutine(LoadPictureEnumerator(imageUrl, width, height, callback));
               });
    }




    public void LocalLoadData()
    {
        //	if (FB.IsLoggedIn) 
        {


            Debug.Log("loading me");
            iGrowth = PlayerPrefs.GetInt("growth");

 	//// restart
	//		iGrowth = 0;

			userViewId = FB.UserId;
            fCurSpeedGrowth = _fSpeedGrowth;
            // first start application
            if (iGrowth == 0)
            {

                //Debug.Log("restart");
				/// load from fb
				/// 

				fScore = (float)(scores[FB.UserId]);
				iGrowth = TreeBehaviour.GetGrowthFromScore( scores[FB.UserId]);
				/// 
                fHumidity = _OPTIM_HUMIDITY;
                timeHumidity = System.DateTime.UtcNow;
                timeChecked = System.DateTime.UtcNow.AddHours(-1);
				timeFertilizerEnd = System.DateTime.UtcNow.AddHours(-1);
				fFertilizerMode = 0f;
                iMoneyCoins = 0;
				iMoneyRubies = 0;
                iThermostate = 0;
                iSoundOff = 0;
                
                iWindowClosed = 0;
                fPotType = 0f;
                fWallType = 0f;
                fCoinsRest = 0f;
				fCoinsFriendRest = 0f;
                fTemperature = 22f;
                fPGrunt = 100f;
                iPotAlert = 0;
                iHoursFromCoinTake = 0;
				iHoursFromCoinFriendTake = 0;
                iAutoWaterDays = 0;
                iSpeed = 1;
                scene.gameObject.SetActive(true);
				oTree.SetTreeGrowth(iGrowth);
				isDataLoaded = true;
				TimeEvents.OnDataLoaded();
                localSaveData();
            }
            else
            {
				/// load from fb
				/// 
				fScore = (float)(scores[FB.UserId]);
			//	iGrowth = 10;
				iGrowth = TreeBehaviour.GetGrowthFromScore( scores[FB.UserId]);
				/// 
                fHumidity = PlayerPrefs.GetFloat("humidity");
                //								iMoney = PlayerPrefs.GetInt ("money");
                timeHumidity = DateTime.Parse(PlayerPrefs.GetString("time_humidity"));
                timeChecked = DateTime.Parse(PlayerPrefs.GetString("time_checked"));
                iThermostate = PlayerPrefs.GetInt("thermostate");
                iWindowClosed = PlayerPrefs.GetInt("is_window_closed");
                fScore = PlayerPrefs.GetFloat("tree1_score");
                iSoundOff = PlayerPrefs.GetInt("is_sound_off");
                fPotType = PlayerPrefs.GetFloat("pot_type");
                fWallType = PlayerPrefs.GetFloat("wall_type");
                fCoinsRest = PlayerPrefs.GetFloat("coins_rest");
				fCoinsFriendRest = PlayerPrefs.GetFloat("coins_fr_rest");
                fTemperature = PlayerPrefs.GetFloat("temperature");
                fPGrunt = PlayerPrefs.GetFloat("grunt");
                iPotAlert = PlayerPrefs.GetInt("num_pot_alert");
                iHoursFromCoinTake = PlayerPrefs.GetInt("hours_coin_take");
				iHoursFromCoinFriendTake = PlayerPrefs.GetInt("hours_coin_fr_take");
                iAutoWaterDays = PlayerPrefs.GetInt("autowater_days");
				timeAutoWaterEnd = DateTime.Parse(PlayerPrefs.GetString("time_autowater"));
				timeFertilizerEnd = DateTime.Parse(PlayerPrefs.GetString("time_fertilizer"));
				fFertilizerMode = PlayerPrefs.GetFloat("fertilizer_mode");

                iSpeed = PlayerPrefs.GetInt("time_speed_test");

                //				float fPrevGrowth = fGrowth > 50 ? fGrowth : 50;
                //
                //
                //
                //								// time in seconds from last save
                //								DateTime nowTime = DateTime.UtcNow;
                //								int days = (int)(nowTime - timeHumidity).TotalDays;
                ////								Debug.Log ("" + iHumidity + "/" + days);
                //								//  days with norm humidity
                //								for (int i = 0; i < days && iHumidity > 0; ++i) {
                //										// tree is growing when humidity is normal
                //										if (isHumidityNormal ()) {
                //												fGrowth += _dayInHours * fCurSpeedGrowth;
                //										}
                //										iHumidity -= 5;
                //								}
                //
                //								int hours = (nowTime - timeHumidity).Hours;
                //
                //				//				Debug.Log ("/" + iHumidity + "/" + fPrevGrowth + "/" + fGrowth);
                //
                //								// calculate current growth of tree
                //								if (isHumidityNormal ()) {
                //										fGrowth += hours * fCurSpeedGrowth;
                //								}

                //load exist couns
                int countsCoinsExist = PlayerPrefs.GetInt("count_coins");



                scene.gameObject.SetActive(true);
				oTree.SetTreeGrowth(iGrowth);
                /// test cons
                //	countsCoinsExist = 2;

                minZCoin = prefabCoin.transform.position.z;
                transformStruct coin = new transformStruct();
                for (int i = 0; i < countsCoinsExist; i++)
                {
                    coin.position.x = PlayerPrefs.GetFloat("coin_x" + i);
                    coin.position.y = PlayerPrefs.GetFloat("coin_y" + i);
                    coin.position.z = PlayerPrefs.GetFloat("coin_z" + i);
                    coin.rotation.x = PlayerPrefs.GetFloat("coin_rx" + i);
                    coin.rotation.y = PlayerPrefs.GetFloat("coin_ry" + i);
                    coin.rotation.z = PlayerPrefs.GetFloat("coin_rz" + i);
                    coin.rotation.w = PlayerPrefs.GetFloat("coin_rw" + i);

					Transform coinObj = null;

					if (coin.position.x < (TreeBehaviour.COIN_X3 - TreeBehaviour.COIN_DX))
					{ 
						// usual coin
						coinObj = Instantiate(prefabCoin, coin.position, coin.rotation) as Transform;
						coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN;
					} else { 
						//friend coin
						coinObj = Instantiate(prefabFriendCoin, coin.position, coin.rotation) as Transform;
						coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN_FRIEND;
					}
                    if (coin.position.z < minZCoin)
                    {
                        minZCoin = coin.position.z;
                    }
					if (coinObj != null && parentCoins != null)
					{
						coinObj.parent = parentCoins;
					}
                }
            }
        }
		Debug.Log("loading me");
		TimeEvents.OnDataLoaded();
		isDataLoaded = true;
    }

    public static void localSaveData()
    {
		Debug.Log("saving me " + userViewId + " " + FB.UserId + " " + isDataLoaded);
		// save local data for me
        if (isDataLoaded && userViewId.CompareTo(FB.UserId) == 0)
        {

			Debug.Log("save");
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("is_window_closed", iWindowClosed);
            PlayerPrefs.SetFloat("tree1_score", fScore);
            PlayerPrefs.SetInt("is_sound_off", iSoundOff);
            PlayerPrefs.SetInt("growth", iGrowth);
            PlayerPrefs.SetFloat("humidity", fHumidity);
            PlayerPrefs.SetString("time_humidity", timeHumidity.ToString());
            PlayerPrefs.SetString("time_checked", timeChecked.ToString());

            //	PlayerPrefs.SetInt("money", iMoney);
            PlayerPrefs.SetInt("thermostate", iThermostate);
            PlayerPrefs.SetFloat("pot_type", fPotType);
            PlayerPrefs.SetFloat("wall_type", fWallType);
            PlayerPrefs.SetFloat("coins_rest", fCoinsRest);
			PlayerPrefs.SetFloat("coins_fr_rest", fCoinsFriendRest);
            PlayerPrefs.SetFloat("temperature", fTemperature);
            PlayerPrefs.SetFloat("grunt", fPGrunt);
            PlayerPrefs.SetInt("num_pot_alert", iPotAlert);
            PlayerPrefs.SetInt("hours_coin_take", iHoursFromCoinTake);
			PlayerPrefs.SetInt("hours_coin_fr_take", iHoursFromCoinFriendTake);
            PlayerPrefs.SetInt("autowater_days", iAutoWaterDays);
			PlayerPrefs.SetString("time_autowater", timeAutoWaterEnd.ToString());
			PlayerPrefs.SetString("time_fertilizer", timeFertilizerEnd.ToString());
			PlayerPrefs.SetFloat("fertilizer_mode", fFertilizerMode);


            PlayerPrefs.SetInt("time_speed_test", iSpeed);


            // save exist coins
            GameObject[] coins = GameObject.FindGameObjectsWithTag("coin");
            int countsCoinsExist = coins.Length;
            PlayerPrefs.SetInt("count_coins", countsCoinsExist);
            for (int i = 0; i < countsCoinsExist; i++)
            {
                PlayerPrefs.SetFloat("coin_x" + i, coins[i].transform.position.x);
                PlayerPrefs.SetFloat("coin_y" + i, coins[i].transform.position.y);
                PlayerPrefs.SetFloat("coin_z" + i, coins[i].transform.position.z);
                PlayerPrefs.SetFloat("coin_rx" + i, coins[i].transform.rotation.x);
                PlayerPrefs.SetFloat("coin_ry" + i, coins[i].transform.rotation.y);
                PlayerPrefs.SetFloat("coin_rz" + i, coins[i].transform.rotation.z);
                PlayerPrefs.SetFloat("coin_rw" + i, coins[i].transform.rotation.w);
            }

        }
    }


	public void LoadUserData(string userId)
	{
		// save previous data
		if (userViewId.CompareTo(FB.UserId) == 0)
		{
			localSaveData();
		} else {
			SaveUserData (userViewId);
		}

		isDataLoaded = false;
		userViewId = userId;
		//destroy all coins
		GameObject[] coins = GameObject.FindGameObjectsWithTag("coin");
		int countsCoinsExist = coins.Length;
		for (int i = 0; i < countsCoinsExist; i++)
		{
			Destroy (coins[i].gameObject, 0);
		}

		// load new
					
		if (userId.CompareTo(FB.UserId) == 0)
		// me
		{
			LocalLoadData();
		}
		else
		//friend
		{

			// temp local init
			fScore = (float)(scores[userId]);
			iGrowth = TreeBehaviour.GetGrowthFromScore( scores[userId]);
			/// 
			fHumidity = _OPTIM_HUMIDITY;
			timeHumidity = System.DateTime.UtcNow;
			timeChecked = System.DateTime.UtcNow;
			timeFertilizerEnd = System.DateTime.UtcNow.AddHours(-1);
			fFertilizerMode = 0f;
			iThermostate = 0;
			iSoundOff = 0;
	//		fScore = 0f;
			iWindowClosed = 0;
			fPotType = 0f;
			fWallType = 0f;
			fCoinsRest = 0f;
			fCoinsFriendRest = 0f;
			fTemperature = 22f;
			fPGrunt = 100f;
			iPotAlert = 0;
			iHoursFromCoinTake = 0;
			iHoursFromCoinFriendTake = 0;
			iAutoWaterDays = 0;
			iSpeed = 1;
			scene.gameObject.SetActive(true);
			oTree.SetTreeGrowth(iGrowth);
			isDataLoaded = true;
			TimeEvents.OnDataLoaded();
			// temp copy of my coins for test

			//load exist couns
			countsCoinsExist = PlayerPrefs.GetInt("count_coins");

			minZCoin = prefabCoin.transform.position.z;
			transformStruct coin = new transformStruct();
			for (int i = 0; i < countsCoinsExist; i++)
			{
				coin.position.x = PlayerPrefs.GetFloat("coin_x" + i);
				coin.position.y = PlayerPrefs.GetFloat("coin_y" + i);
				coin.position.z = PlayerPrefs.GetFloat("coin_z" + i);
				coin.rotation.x = PlayerPrefs.GetFloat("coin_rx" + i);
				coin.rotation.y = PlayerPrefs.GetFloat("coin_ry" + i);
				coin.rotation.z = PlayerPrefs.GetFloat("coin_rz" + i);
				coin.rotation.w = PlayerPrefs.GetFloat("coin_rw" + i);
				
				Transform coinObj = null;
				
				if (coin.position.x < (TreeBehaviour.COIN_X3 - TreeBehaviour.COIN_DX))
				{ 
					// usual coin
					coinObj = Instantiate(prefabCoin, coin.position, coin.rotation) as Transform;
					coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN;
				} else { 
					//friend coin
					coinObj = Instantiate(prefabFriendCoin, coin.position, coin.rotation) as Transform;
					coinObj.gameObject.GetComponent<CoinScript>().costCoin = DataLoader._COST_COIN_FRIEND;
				}
				if (coin.position.z < minZCoin)
				{
					minZCoin = coin.position.z;
				}
				if (coinObj != null && parentCoins != null)
				{
					coinObj.parent = parentCoins;
				}
			}
		}

	}

	public void SaveUserData(string userId)
	{

	}

    void OnApplicationQuit()
    {
        localSaveData();
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            localSaveData();
        }
    }





}











