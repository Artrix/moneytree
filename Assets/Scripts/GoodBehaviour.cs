using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;
using UnityEngine.UI;




public class GoodBehaviour : MonoBehaviour {

	//public string goodId;// =  EditorGUILayout.Popup ;
	public int good_Id;// =  EditorGUILayout.Popup ;
	public Text goodLabel;
	public Text coinsLabel;
	public Text rubiesLabel;
	public Image goodImage;


	public BuyInMarket winBuy;
	public bool isOnlyOneGood;

	const float _COINS_TO_RUBIES = 150;

	private string goodId;

	// Use this for initialization
	void Start () {
		goodId = PurchasedAssets.goodIds[good_Id];
        Debug.Log(goodId);
	}
	
	// Update is called once per frame
	void Update () {
	}



	public void OnClick()
	{
		string goodText = "good";
		if (goodLabel != null) {
			goodText = goodLabel.text;
		}
		winBuy.LoadGood (good_Id, goodText, goodImage.sprite, coinsLabel.text, rubiesLabel.text);
	}
}
