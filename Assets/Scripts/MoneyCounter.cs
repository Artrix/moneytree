using UnityEngine;
using System.Collections;
using Soomla.Store;
using UnityEngine.UI;

public class MoneyCounter : MonoBehaviour {
	public Text labelCoins ;
	public Text labelRubies;

	// Use this for initialization
	void OnEnable () {
//		labelCoins.text = "" + StoreInventory.GetItemBalance(PurchasedAssets.COINS_ITEM_ID);
		StoreInventory.BuyItem (PurchasedAssets.TEST_ITEM_ID);

	}
	
	// Update is called once per frame
	void OnGUI () {
		labelCoins.text = "" + DataLoader.iMoneyCoins;//StoreInventory.GetItemBalance(PurchasedAssets.COINS_ITEM_ID);
		labelRubies.text = "" + DataLoader.iMoneyRubies;
	}
}
