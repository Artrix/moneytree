using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FriendBehaviour : MonoBehaviour {

	public RawImage avatar;
	public Text userName;
	public Text userLevel;
	public DataLoader data;
	public Toggle togButton;
	private string userId = "";
	private bool isPicture = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}


	public void OnClick ()

	{
		if (togButton.isOn && userId.CompareTo(DataLoader.userViewId) != 0)
		{
			DataLoader.oUser = this;
			DataLoader.data.LoadUserData(userId);

		}
        //Debug.Log("click2");
	}

	public void SetUserId(string id)
	{
		userId = id;
		togButton.group = transform.parent.GetComponent<ToggleGroup>();
		if (userId.CompareTo(FB.UserId) == 0)
		{
			togButton.isOn = true;
		}
		Debug.Log ("avatar " + userId);
		if (DataLoader.friendImages.ContainsKey (userId)) {

			Texture tex;
			isPicture = DataLoader.friendImages.TryGetValue(userId, out tex);
			Debug.Log (userId + " avatar0 " + isPicture);
			avatar.texture = tex;
			avatar.enabled = isPicture;
		}
		else {
			Debug.Log (userId + " avatar1 " + Util.GetPictureURL(userId, DataLoader._AVATAR_WIDTH,  DataLoader._AVATAR_HEIGHT));
			// We don't have this players image yet, request it now
			data.LoadPicture(Util.GetPictureURL(userId,  DataLoader._AVATAR_WIDTH,  DataLoader._AVATAR_HEIGHT),  DataLoader._AVATAR_WIDTH,  DataLoader._AVATAR_HEIGHT, pictureTexture =>
			            {
				Debug.Log (userId + " avatar1 " + (pictureTexture != null));

				if (pictureTexture != null)
				{
					if (DataLoader.friendImages.ContainsKey (userId)) {
						Texture tex;
						isPicture = DataLoader.friendImages.TryGetValue(userId, out tex);
						Debug.Log (userId + " avatar2 " + isPicture);
						avatar.texture = tex;
						avatar.enabled = isPicture;
					} else {

						DataLoader.friendImages.Add(userId, pictureTexture);

						avatar.texture = pictureTexture;
						avatar.enabled = true;
						isPicture = true;
						Debug.Log (userId + " avatar3 " + isPicture);
					}
				}
			});
		}
	}

	public void SetUserName(string name)
	{
		userName.text = name;
	}

	public void SetUserLevel(int level)
	{
		userLevel.text = (level+1).ToString();
	}

}
