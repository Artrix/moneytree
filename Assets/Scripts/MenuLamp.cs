using UnityEngine;
using System.Collections;

public class MenuLamp : MonoBehaviour {

	public GameObject lightLampPoint;
	public GameObject lightLampSpot;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick()
	{
		if (lightLampSpot.GetComponent<Light>().enabled)
		{
			lightLampPoint.GetComponent<Light>().enabled = false;
			lightLampSpot.GetComponent<Light>().enabled = false;
			DataLoader.colorText.a = 0.8f;
		} else {
			lightLampPoint.GetComponent<Light>().enabled = true;
			Light spot = lightLampSpot.GetComponent<Light>();
			spot.enabled = true;
			float intensity = spot.intensity;
			spot.intensity = intensity * 0.5f;
			spot.intensity = intensity;
			DataLoader.colorText.a = 1.0f;
		}

	}
}
