﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class _RestartButton : MonoBehaviour {

	private DataLoader data;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClick()
	{

//		MoneyBehaviour coins = FindObjectsOfType(MoneyBehaviour);
//		foreach (MoneyBehaviour coin in coins)
//		{
//			Destroy (coin.coin, 0);
//		}

		LocalStoreInfo.ResetBalances ();

		DataLoader.iGrowth = 0;
		DataLoader.fScore = 0f;
		if (FB.IsLoggedIn && DataLoader.isDataLoaded) {
			Debug.Log("post score");
			var query = new Dictionary<string, string>();
			query["score"] = ((int)DataLoader.fScore).ToString();
			FB.API("/me/scores", Facebook.HttpMethod.POST, delegate(FBResult r) 
			{
				Debug.Log("Result score: " + r.Text);

			}, query);
			DataLoader.localSaveData ();
			Application.Quit();
		}
		
		
//		

	}
}
